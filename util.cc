#include "util.h"

#include <cassert>

namespace fit
{

std::string get_order_book_id(const std::string& wind_code)
{
  auto suffix = wind_code.substr(7);
  auto stock_code = wind_code.substr(0, 7);
  assert(suffix == "SZ" || suffix == "SH");
  if(suffix == "SH")
  {
    return stock_code + "XSHG";
  }
  else
  {
    return stock_code + "XSHE";
  }
}

std::string get_stock_code(const std::string& wind_code)
{
  return wind_code.substr(0, 6);
}

}
