#pragma once

#include <muduo/net/TcpServer.h>

#include "mongo_helper.h"

namespace zy
{
class fit_data_server : boost::noncopyable
{
  public:
    fit_data_server(muduo::net::EventLoop* loop, const muduo::net::InetAddress& addr, std::unique_ptr<mongo_helper>&& helper);

    typedef muduo::net::TcpConnectionPtr TcpConnectionPtr;
    typedef muduo::net::Buffer Buffer;
    typedef muduo::Timestamp Timestamp;

    void onConnection(const TcpConnectionPtr& con);

    void onMessage(const TcpConnectionPtr& con, Buffer* buf, Timestamp receiveTime);

    void start() { server_.start(); }

  private:
    muduo::net::EventLoop * loop_;
    muduo::net::TcpServer server_;
    std::unique_ptr<mongo_helper> mongo_helper_;
    TcpConnectionPtr clientCon_;
};
}

