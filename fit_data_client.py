# encoding: utf-8
import socket
import zlib
import struct
import datetime
import pandas as pd


def unpack_int(data):
    return int(struct.unpack("!i", data)[0])


def get_date(integer_date):
    day = int(integer_date % 100)
    integer_date /= 100
    month = int(integer_date % 100)
    integer_date /= 100
    year = int(integer_date)
    return datetime.date(year, month, day) + datetime.timedelta(1)


def pack_int(integer):
    return struct.pack("!i", integer)


def compress(input_data):
    return zlib.compress(input_data.encode(), 9)


def send_data(json_str, sockfd):
    print("origin_len", len(json_str))
    compress_data = compress(json_str)
    length = len(compress_data)
    print("compressed len", length)
    sockfd.sendall(pack_int(length))
    sockfd.sendall(compress_data)


def get_stock_list():
    df_instruments = all_instruments(type="CS")
    stocks = list(df_instruments["order_book_id"])
    stocks = list(map(lambda x : str(x), stocks))
    return stocks

def read_int(sockfd):
    data = bytes()
    while len(data) < 4:
        more = sockfd.recv(4 - len(data))
        if not more:
            print("recv error!")
            exit(-1)
        else:
            data += more
    return data


def get_wind_code(order_book_id):
    stock_code = order_book_id[0:6]
    if order_book_id[9:] == "HG":
        return stock_code + ".SH"
    return stock_code + ".SZ"

if __name__ == "__main__":
    sockfd = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    errno = sockfd.connect_ex(("58.248.156.244", 8088))
    if errno != 0:
        print ("connect to remote server error!")
        exit(-1)
    net_integer = read_int(sockfd)
    begin_integer_date = unpack_int(net_integer)
    begin_date = get_date(begin_integer_date)
    begin_date_str = begin_date.strftime('%Y-%m-%d')
    end_date = datetime.datetime.now().date()
    end_date_str = end_date.strftime('%Y-%m-%d')

    stock_list = get_stock_list()
    list_dbs = []
    i = 0
    for stock in stock_list:
        df_stock = get_price(stock, start_date = begin_date_str, end_date = end_date_str, adjust_type = "none",
                             frequency = "1m", skip_suspended = True)
        if len(df_stock) == 0:
            continue
        df_stock["date"] = df_stock.index.map(lambda x: x.year * 10000 + x.month * 100 + x.day)
        df_stock["time"] = df_stock.index.map(lambda x: x.hour * 10000 + x.minute * 100 + x.second)
        df_stock["order_book_id"] = stock
        df_stock["stock_code"] = stock[0:6]
        df_stock["wind_code"] = get_wind_code(stock)
        # fill values forward
        df_stock = df_stock.fillna(method = "pad")
        df_stock = df_stock.fillna(method = "bfill")
        list_dbs.append(df_stock)
        i += 1
        if i % 300 == 0:
            print(float(i) / len(stock_list), " percent complete")
    df_all = pd.concat(list_dbs, ignore_index = True)
    json_str = df_all.to_json(orient="records")
    print ("begin to send data")
    send_data(json_str, sockfd)
