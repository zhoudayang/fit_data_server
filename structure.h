#pragma once

namespace zy
{

struct min_data
{
  double open;
  double close;
  double high;
  double low;
  double volume;
  double amt;
};

}
