#include "json_server.h"
#include "zlib_wrapper.h"

#include <muduo/net/EventLoop.h>
#include <muduo/base/Logging.h>
#include "zlib_wrapper.h"

using namespace zy;
using std::placeholders::_1;
using std::placeholders::_2;
using std::placeholders::_3;

fit_data_server::fit_data_server(muduo::net::EventLoop* loop, const muduo::net::InetAddress& addr, std::unique_ptr<mongo_helper>&& helper)
  : loop_(loop),
    server_(loop_, addr, "fit_data_server"),
    mongo_helper_(std::move(helper)),
    clientCon_()
{
  server_.setConnectionCallback(std::bind(&fit_data_server::onConnection, this, _1));
  server_.setMessageCallback(std::bind(&fit_data_server::onMessage, this, _1, _2, _3));
}

// only allow one client enter into the server
void fit_data_server::onConnection(const fit_data_server::TcpConnectionPtr &con)
{
  LOG_INFO << "connection from " << con->peerAddress().toIpPort() << "is " << (con->connected() ? "up" : "down");
  if(con->connected())
  {
    if(mongo_helper_->insert_now() || clientCon_)
      con->shutdown();
    else
      clientCon_ = con;
    int date = mongo_helper_->lastest_update_date();
    if(date == -1)
    {
      LOG_FATAL << "error update date ";
    }
    date = muduo::net::sockets::hostToNetwork32(date);
    con->send(&date, sizeof(int32_t));
  }
  else
  {
    if(con == clientCon_)
    {
      clientCon_.reset();
    }
  }
}

void fit_data_server::onMessage(const fit_data_server::TcpConnectionPtr &con,
                                fit_data_server::Buffer *buf,
                                fit_data_server::Timestamp receiveTime)
{
  while(buf ->readableBytes() > sizeof(int32_t) && (buf->readableBytes() >= static_cast<size_t>(buf->peekInt32() + sizeof(int32_t))))
  {
    int32_t length = buf->readInt32();
    std::string data(buf->peek(), length);
    buf->retrieve(length);
    ZlibDecompressor decompressor(data);
    if(!decompressor.valid())
    {
      LOG_ERROR << "decompress data error! due to " << data;
      buf->retrieveAll();
      con->shutdown();
      return;
    }
    else
    {
      mongo_helper_->insert(decompressor.output());
    }
  }
}

