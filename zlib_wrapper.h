#pragma once

#include <boost/noncopyable.hpp>
#include <zlib.h>
#include <string>

namespace zy
{
// for decompress data receive from network
class ZlibDecompressor : boost::noncopyable
{
  public:
    explicit ZlibDecompressor(const std::string& input);

    const char* zlibErrorMessage() const { return zstream_.msg; }

    ~ZlibDecompressor();

    std::string output() const { return output_; }

    bool valid() const { return z_init_error_ == Z_OK && (zerror_ == Z_OK || zerror_ == Z_STREAM_END); }

  private:
    const static int CHUNK = 8192;

    int decompress();

    z_stream zstream_;
    int zerror_;
    int z_init_error_;
    std::string input_;
    std::string output_;
};
}
