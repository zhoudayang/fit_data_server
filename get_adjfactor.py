#!/usr/bin/env python
# encoding: utf-8

import pandas as pd

# get stock list
def get_stock_list():
    df_instruments = all_instruments(type = "CS")
    stocks = list(df_instruments["order_book_id"])
    stocks = list(map(lambda x : str(x), stocks))
    return stocks

if __name__ == "__main__":
    stock_list = get_stock_list()
    end_date = datetime.datetime().now().date()
    end_date_str = end_date.strftime("%Y-%m-%d")
    list_adjfactor = []
    for stock in stock_list:
        df_adjfactor = get_ex_factor(stock, start_date='1900-01-01', end_date = end_date_str)
        if len(df_adjfactor) == 0:
            continue
        df_adjfactor["order_book_id"] = stock
        df_adjfactor["ex_end_date"] = df_adjfactor["ex_end_date"].map(lambda x : int(x.year * 10000 + x.month * 100 + x.day))
        df_adjfactor["ex_date"] = df.index.map(lambda x : int(x.year * 10000 + x.month * 100 + x.day))
        df_adjfactor = df_adjfactor.reset_index(drop=True)
        list_adjfactor.append(df_adjfactor)
    df_all = pd.concat(list_adjfactor)
    print (df_all)
