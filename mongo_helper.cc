#include "mongo_helper.h"

#include "util.h"

#include <mongocxx/instance.hpp>
#include <muduo/base/Logging.h>
#include <rapidjson/document.h>

namespace instance
{
class mongo_initializer
{
 public:
  mongo_initializer()
  {
    mongocxx::instance instance{};
  }
};
const static mongo_initializer mongo_initializer;
}

namespace detail
{
inline int32_t time_convert(int32_t time)
{
  time /= 100;
  int32_t value = time % 100;
  time /= 100;
  value += 60 * time;
  value -= 1;
  return (value % 60 + (value / 60) * 100) * 100;
}
}

using namespace zy;
using bsoncxx::builder::stream::document;
using bsoncxx::builder::stream::open_document;
using bsoncxx::builder::stream::close_document;
using bsoncxx::builder::stream::finalize;
using bsoncxx::builder::basic::kvp;
using bsoncxx::builder::basic::sub_array;

mongo_helper::mongo_helper(const std::string &uri,
                           const std::string &db,
                           const std::string &store_coll,
                           const std::string &record) :
  uri_(uri),
  client_(uri_),
  store_coll_name_(store_coll),
  data_(),
  max_date_(-1),
  docs_(),
  insert_now_(false)
{
  auto database = client_[db];
  store_coll_ = database[store_coll_name_];
  record_coll_ = database[record];
}

int32_t mongo_helper::lastest_update_date()
{
  auto maybe_result = record_coll_.find_one(document{} << "collection" << store_coll_name_ <<finalize);
  if(maybe_result)
  {
    auto result = (*maybe_result).view();
    int32_t update_date = result["update_date"].get_int32().value;
    return update_date;
  }
  return -1;
}

void mongo_helper::record_update_date(int32_t date)
{
  document filter_builder, update_builder;
  filter_builder << "collection" << store_coll_name_;
  update_builder << "$set" << open_document <<  "update_date" << date << close_document;
  record_coll_.update_one(filter_builder.view(), update_builder.view());
}

// todo: serialize json string
void mongo_helper::insert(std::string &&update_data)
{
  insert_now_ = true;
  data_.clear();
  rapidjson::Document document;
  document.Parse(update_data.c_str());
  assert(document.IsArray());
  // store data into data_
  for(auto it = document.Begin(); it != document.End(); ++it)
  {
    const auto& doc = it->GetObject();

    std::string wind_code = doc["wind_code"].GetString();
    auto date = doc["date"].GetInt();
    auto time = doc["time"].GetInt();
    // time统一减去1，时间从9:30 ～ 14:59
    time = detail::time_convert(time);

    auto open = doc["open"].GetDouble();
    auto close = doc["close"].GetDouble();
    auto high = doc["high"].GetDouble();
    auto low = doc["low"].GetDouble();
    auto amt = doc["total_turnover"].GetDouble();
    auto volume = doc["volume"].GetDouble();

    auto& container = data_[wind_code][date];
    // advanced structure initialize way
    container[time] = {.open = open, .close = close, .high = high, .low = low, .volume = volume, .amt = amt};
    if(container.size() >= MINUTES_IN_DAY)
    {
      insert_helper(wind_code, date);
    }
    max_date_ = std::max(max_date_, date);
  }
  // 做好善后工作
  rehabilitation();
}

void mongo_helper::insert_helper(const std::string &wind_code, int32_t date)
{
  auto stock_code = fit::get_stock_code(wind_code);
  auto order_book_id = fit::get_order_book_id(wind_code);
  const auto& day_data = data_[wind_code][date];
  bsoncxx::builder::basic::document builder{};
  builder.append(kvp("wind_code", wind_code));
  builder.append(kvp("date", date));
  builder.append(kvp("order_book_id", order_book_id));
  builder.append(kvp("stock_code", stock_code));

  builder.append(kvp("time", [&day_data](sub_array child) {
        for(const auto& elem: day_data)
        {
          child.append(elem.first);
        }
  }));

 builder.append(kvp("open", [&day_data](sub_array child) {
        for(const auto& elem: day_data)
        {
          child.append(elem.second.open);
        }
  }));

  builder.append(kvp("close", [&day_data](sub_array child) {
        for(const auto& elem: day_data)
        {
          child.append(elem.second.close);
        }
  }));

  builder.append(kvp("high", [&day_data](sub_array child) {
        for(const auto& elem: day_data)
        {
          child.append(elem.second.high);
        }
  }));

  builder.append(kvp("low", [&day_data](sub_array child) {
        for(const auto& elem: day_data)
        {
          child.append(elem.second.low);
        }
  }));

  builder.append(kvp("volume", [&day_data](sub_array child) {
        for(const auto& elem: day_data)
        {
          child.append(elem.second.volume);
        }
  }));

  builder.append(kvp("amt", [&day_data](sub_array child) {
        for(const auto& elem: day_data)
        {
          child.append(elem.second.amt);
        }
  }));

  insert(builder.extract());
  data_[wind_code].erase(date);
}

void mongo_helper::insert(bsoncxx::document::value&& doc, bool last)
{
  const static int MAX_SIZE = 500;
  docs_.push_back(std::move(doc));
  if(docs_.size() >= MAX_SIZE || (last && !docs_.empty()))
  {
    store_coll_.insert_many(docs_);
    LOG_INFO << "insert " << docs_.size() << " collection into mongodb!";
    docs_.clear();
  }
}

void mongo_helper::rehabilitation()
{
  for(auto& elem : data_)
  {
    const auto& wind_code = elem.first;
    auto& container = elem.second;
    for(auto& value : container)
    {
      auto date = value.first;
      auto& datas = value.second;
      assert(!datas.empty());
      if(datas.size() != MINUTES_IN_DAY)
      {
        LOG_INFO << wind_code << " : " << date << " value's size != " << MINUTES_IN_DAY << " which is " << datas.size();
      }
      insert_helper(wind_code, date);
    }
  }
  record_update_date(max_date_);
  data_.clear();
  std::unordered_map<std::string, std::unordered_map<int32_t, std::map<int32_t, struct min_data>> > temp_data;
  data_.swap(temp_data);
  insert_now_ = false;
}
