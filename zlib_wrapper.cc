#include "zlib_wrapper.h"

#include <assert.h>
#include <strings.h>

using namespace zy;

ZlibDecompressor::ZlibDecompressor(const std::string& input)
  : zerror_(Z_OK),
    z_init_error_(Z_OK),
    input_(input),
    output_()
{
  ::bzero(&zstream_, sizeof(zstream_));
  z_init_error_ = inflateInit(&zstream_);
  if(z_init_error_ == Z_OK)
    zerror_ = decompress();
}

ZlibDecompressor::~ZlibDecompressor()
{
  if(z_init_error_ == Z_OK)
    ::inflateEnd(&zstream_);
}

int ZlibDecompressor::decompress()
{
  int ret;
  size_t begin = 0;
  size_t size = input_.size();
  // output buffer
  unsigned char out[CHUNK];
  do
  {
    int chunk = ((size - begin) < CHUNK ? size - begin : CHUNK);
    if(chunk == 0)
      break;
    zstream_.avail_in = static_cast<uint>(chunk);
    zstream_.next_in = (Bytef*)(&input_[begin]);
    do
    {
      zstream_.avail_out = CHUNK;
      zstream_.next_out = (Bytef*)(out);
      ret = ::inflate(&zstream_, Z_NO_FLUSH);
      assert(ret != Z_STREAM_ERROR);
      switch(ret)
      {
        case Z_NEED_DICT:
          ret = Z_DATA_ERROR;
        case Z_DATA_ERROR:
        case Z_MEM_ERROR:
          return ret;
      }
      int have = CHUNK - static_cast<int>(zstream_.avail_out);
      output_.append(out, out + have);
    }while(zstream_.avail_out == 0); // available output buffer is none
    begin += chunk;
  }while(ret != Z_STREAM_END);
  return ret;
}
