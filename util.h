#pragma once

#include <string>

namespace fit
{
  std::string get_order_book_id(const std::string& wind_code);
  std::string get_stock_code(const std::string& wind_code);
}
