#include "json_server.h"

#include <unistd.h>
#include <muduo/net/EventLoop.h>
#include <muduo/base/LogFile.h>
#include <muduo/base/Logging.h>

using namespace zy;

std::unique_ptr<muduo::LogFile> g_logFile;

void outputFunc(const char* msg, int len)
{
  g_logFile->append(msg, len);
}

void flushFunc()
{
  g_logFile->flush();
}

void init_log()
{
  g_logFile.reset(new muduo::LogFile("/tmp/fit_data_server", 512 * 1024, false, 3, 1));
  muduo::Logger::setOutput(outputFunc);
  muduo::Logger::setFlush(flushFunc);
  muduo::Logger::setLogLevel(muduo::Logger::DEBUG);
}


int main()
{

  if(daemon(0, 0) == -1)
  {
    fprintf(stderr, "create daemon process error!\n");
    abort();
  }

  init_log();
  LOG_INFO << "pid = " << ::getpid();

  muduo::net::EventLoop loop;

  std::string mongo_uri = "mongodb://192.168.0.117:27017";
  std::string mongo_db = "A_stock_min";
  std::string store_collection = "A_stock_min";
  std::string update_record = "update_record";
  std::unique_ptr<mongo_helper> helper(new mongo_helper(mongo_uri, mongo_db, store_collection, update_record));

  fit_data_server server(&loop, muduo::net::InetAddress("0.0.0.0", 8088), std::move(helper));
  server.start();

  loop.loop();

  return 0;
}
