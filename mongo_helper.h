#pragma once

#include <boost/noncopyable.hpp>
#include <string>
#include <mongocxx/client.hpp>
#include <mongocxx/uri.hpp>
#include <bsoncxx/builder/basic/document.hpp>
#include <unordered_map>
#include <vector>
#include <map>

#include "structure.h"

namespace zy
{
class mongo_helper : boost::noncopyable
{
 public:
  mongo_helper(const std::string& uri, const std::string& db, const std::string& store_coll, const std::string& record);

  ~mongo_helper() = default;

  // get lastest_update_date
  int32_t lastest_update_date();

  // update update_date
  void record_update_date(int32_t date);

  void insert(std::string&& update_data);

  bool insert_now() const { return insert_now_; }

 private:

  void insert_helper(const std::string& wind_code, int32_t date);

  void insert(bsoncxx::document::value&& doc, bool last = false);

  // 善后工作
  void rehabilitation();

  const static int MINUTES_IN_DAY = 240;

  mongocxx::uri uri_;
  mongocxx::client client_;
  std::string store_coll_name_;
  mongocxx::collection store_coll_;
  mongocxx::collection record_coll_;
  // 最新数据日期
  int max_date_;
  std::unordered_map<std::string, std::unordered_map<int32_t, std::map<int32_t, struct min_data>> > data_;
  std::vector<bsoncxx::document::value> docs_;
  bool insert_now_;
};
}
